---
title: "Towards a book"
site: bookdown::bookdown_site
output:
  bookdown::pdf_document2:
    toc: yes
---

# Preface

This will combine all Rmd files in alphabetical order.

You can also add some math equations:

$$
C(u,v) = u \cdot v
$$ 

denotes the bivariate independence copula.